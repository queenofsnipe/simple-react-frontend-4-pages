import React, { Component } from "react";
import $ from 'jquery';
import { InnerHeader } from "components/InnerHeader.jsx";
import { SitebarArea } from "components/SitebarArea.jsx";

import { AccountMainArea } from "components/Account/AccountMainArea.jsx";
import LogoutModal from "../components/LogoutModal";

export class Account extends Component{

    constructor(props){
        super(props);
        this.state={

        }
    }

    render(){
        return(
            <body className="inner_bg">
                <InnerHeader></InnerHeader>
                <SitebarArea></SitebarArea>
                <AccountMainArea></AccountMainArea>
                <LogoutModal></LogoutModal>
                
            </body>
        )
    }
}

export default Account;