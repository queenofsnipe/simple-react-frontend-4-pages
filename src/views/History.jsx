import React, { Component } from "react";
import $ from 'jquery';
import { InnerHeader } from "components/InnerHeader.jsx";
import { SitebarArea } from "components/SitebarArea.jsx";
import LogoutModal from "../components/LogoutModal";
import { HistoryMainArea } from "../components/History/HistoryMainArea";
export class History extends Component{

    constructor(props){
        super(props);
        this.state={

        }
    }

    render(){
        return(
            <body className="inner_bg">
                <InnerHeader></InnerHeader>
                <SitebarArea></SitebarArea>
                <HistoryMainArea></HistoryMainArea>
                <LogoutModal></LogoutModal>
                
            </body>
        )
    }
}

export default History;