import React, { Component } from "react";
import $ from 'jquery';
import { InnerHeader } from "components/InnerHeader.jsx";
import { SitebarArea } from "components/SitebarArea.jsx";
import { BuyPurchaseBox } from "components/Buy/BuyPurchaseBox.jsx";
import { BuyLivesBox } from "components/Buy/BuyLivesBox.jsx";
import { BuyPaymentBox } from "components/Buy/BuyPaymentBox.jsx";
import { LogoutModal } from "components/LogoutModal.jsx";
import { BuySummaryModal } from "components/Buy/BuySummaryModal.jsx";



// import {jquery} from "assets/js/jquery-3.3.1.slim.min.js";

// import {bootstrapjs} from "assets/js/bootstrap.min.js";

class Buy extends Component{
    render(){       
        return(
            <div className="inner_bg">
                <InnerHeader></InnerHeader>
                <SitebarArea></SitebarArea>
                <div className="inner_main_area">
                    <div className="buy_main_box">
                        <div className="buy_sec_1">
                            <div className="row">
					            <div className="col-md-8">
                                    <BuyPurchaseBox header_text="How many lives would you like to purchase?"></BuyPurchaseBox>
                                </div>
                                <div className="col-md-4">
                                    <BuyLivesBox currency="£" items="4" price="4"></BuyLivesBox>
                                </div>
                            </div>
                        </div>
                        <div className="buy_sec_2">
                            <BuyPaymentBox></BuyPaymentBox>
                        </div>
                    </div>
                </div>
                <LogoutModal></LogoutModal>
                <BuySummaryModal></BuySummaryModal>
            </div>
            );
    }
}

$(document).ready(function() {
	$('.minus').click(function () {
		var $input = $(this).parent().find('input');
		var count = parseInt($input.val()) - 1;
		count = count < 1 ? 1 : count;
		$input.val(count);
		$input.change();
		return false;
	});
	$('.plus').click(function () {
		var $input = $(this).parent().find('input');
		$input.val(parseInt($input.val()) + 1);
		$input.change();
		return false;
	});
});

export default Buy;
