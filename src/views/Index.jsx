import React, { Component } from "react";
import $ from 'jquery';


import { HomeNavbar } from "components/Home/HomeNavbar.jsx";
import { HeroArea } from "components/Home/HeroArea.jsx";
import { HomeVideoArea } from "components/Home/HomeVideoArea.jsx";
import { HomePlayArea } from "components/Home/HomePlayArea.jsx";
import { HomeAboutUsArea } from "components/Home/HomeAboutUsArea.jsx";
import { Footer } from "components/Home/Footer.jsx";
import { HomeCreateAccountModal } from "components/Home/HomeCreateAccountModal.jsx";
import { AppArea } from "components/Home/AppArea.jsx";







class Index extends Component{
    constructor(props){
        super(props);
        this.state = {
        };
    }

    render(){
        return(
            <body>
                <header className="site_header">
                    <div className="container">
                        <div className="row">
                            <div className="col-sm-12">
                                <HomeNavbar></HomeNavbar>
                            </div>
                        </div>
                    </div>
                </header>
                <HeroArea></HeroArea>
                <HomeVideoArea></HomeVideoArea>
                <HomePlayArea></HomePlayArea>
                <section className="get_training">
                    <div className="container">
                        <div className="row">
                            <div className="col-sm-12">
                                <div className="get_training_content">
                                    <h3>Get training… Get winning…</h3>
                                    <p>Your fate is in your hands…</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <HomeAboutUsArea></HomeAboutUsArea>
                <AppArea></AppArea>
                <Footer></Footer>

                <HomeCreateAccountModal></HomeCreateAccountModal>
            </body>
        );
        
    }


}
export default Index;

