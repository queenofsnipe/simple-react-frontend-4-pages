
import Buy from "views/Buy.jsx";
import Index from "views/Index.jsx";
import Account from "views/Account";
import History from "views/History";


const dashboardRoutes = [
  {
    path: "/buy",
    name: "Buy",
    component: Buy,
    layout: "/admin"
  },
  {
    path: "/index",
    name: "Index",
    component: Index,
    layout: "/admin"
  },

  {
    path: "/account",
    name: "Account",
    component: Account,
    layout: "/admin"
  },
  {
    path: "/history",
    name: "History",
    component: History,
    layout: "/admin"
  }

];

export default dashboardRoutes;
