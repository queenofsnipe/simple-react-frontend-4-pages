import React from "react";
import ReactDOM from "react-dom";

import $ from "jquery";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";

import "assets/css/bootstrap.min.css";
import "assets/font-awsome/font-awsome-v5.7.2.css";
import "assets/fonts/fonts.css";
import "assets/css/style.css";
import "assets/css/responsive.css";


import AdminLayout from "layouts/Admin.jsx";

ReactDOM.render(
  <BrowserRouter>
    <Switch>
      <Route path="/" render={props => <AdminLayout {...props} />}  />
      <Redirect from="/" to="/admin/index" />
    </Switch>
  </BrowserRouter>,
  document.getElementById("root")
);

$(".inner_toggle").click(function(){
  $("body").toggleClass("site_nav_open");
});
