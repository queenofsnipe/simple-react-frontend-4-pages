import React, { Component } from "react";
import logo from "assets/images/logo-black.png";

export class SitebarArea extends Component{
    render(){
        return (
            <div className="sitebar_area">
                <div className="inner_brand">
                    <a href="/admin/index">
                        <img src={ logo } alt="logo"/>
                    </a>
                </div>
                <div className="sitebar_nav">
                    <ul>
                        <li>
                            <a href="/admin/account">Account Details</a>
                        </li>
                        <li>
                            <a href="/admin/buy">Buy Lives</a>
                        </li>
                        <li className="active">
                            <a href="/admin/history">View Purchase History</a>
                        </li>
                        <li>
                            <a href="" data-toggle="modal" data-target="#logut_modal">Log Out</a>
                        </li>
                    </ul>
                </div>
            </div>
	
        ); 

    }
}
export default SitebarArea;