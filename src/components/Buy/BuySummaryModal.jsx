import React, { Component } from "react";
export class BuySummaryModal extends Component{
    constructor(props) {
        super(props);
        this.state = {
        };
    }
    render(){
        return (
            <div className="modal fade summary_modal" id="summary_modal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-body">
        
                            <div className="account_title">
                                <h4>Summary</h4>
                            </div>
                            
                            <div className="account_form">
                                <form>
                                    <div className="form_purchase_lives">
                                        <p>Purchase 4 Lives <span>£4</span></p>
                                    </div>
                                    <div className="form_purchase_total">
                                        <p>Total <span>£4</span></p>
                                    </div>
                                    <div className="row form_footer account_form_footer">
                                        <div className="col-sm-12">
                                            <div className="signup_btn">
                                                <button type="submit" className="btn btn-primary">
                                                    Pay now
                                                    <span className="btn_arrow"><i className="fas fa-arrow-circle-right"></i></span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div className="payment_secured">
                        <p>Payment secured by abc.</p>
                    </div>
                </div>
            </div>
        ); 

    }
}
export default BuySummaryModal;