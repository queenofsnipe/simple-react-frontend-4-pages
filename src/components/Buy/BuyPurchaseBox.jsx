import React, { Component } from "react";
export class BuyPurchaseBox extends Component{
    constructor(props) {
        super(props);
        this.state = {
        };
    }
    render(){
        return (
          <div className="buy_purchase_box">
              <h5>{ this.props.header_text }</h5>
              <div className="number">
                <span className="minus">-</span>
                <input type="text" defaultValue="4"/>
                <span className="plus">+</span>
              </div>
          </div>  
	
        ); 

    }
}
export default BuyPurchaseBox;