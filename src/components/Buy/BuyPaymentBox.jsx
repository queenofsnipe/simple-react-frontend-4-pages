import React, { Component } from "react";
export class BuyPaymentBox extends Component{
    constructor(props) {
        super(props);
        this.state = {
        };
    }
    render(){
        return (
            <div className="buy_payment_box">
                <div className="account_title">
                    <h4>Payment</h4>
                </div>
                <div className="account_form">
                    <form>
                        <div className="row">
                            <div className="col-sm-12">
                                <div className="form-group">
                                    <label htmlFor="Card-Number">Card Number</label>
                                    <input type="text" className="form-control" id="Card-Number" placeholder="Card Number" defaultValue="1407448913464646" />
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-8">
                                <div className="form-group">
                                    <label>Expiry Date</label>
                                    <div className="row">
                                        <div className="col-6">
                                            <div className="input-group">
                                                <div className="input-group-prepend">
                                                    <input type="text" className="form-control" placeholder="Day" defaultValue="01" />
                                                </div>
                                                <div className="input-group-append">
                                                    <input type="text" className="form-control" placeholder="Month" defaultValue="MM" disabled="" />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-6">
                                            <div className="input-group">
                                                <div className="input-group-prepend">
                                                    <input type="text" className="form-control" placeholder="Year" defaultValue="2021" />
                                                </div>
                                                <div className="input-group-append">
                                                    <input type="text" className="form-control" placeholder="YYYY" defaultValue="YYYY" disabled="" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-4">
									<div className="form-group">
										<label htmlFor="CVV">CVV</label>
										<input type="text" className="form-control" id="CVV" placeholder="CVV" defaultValue="147"/>
									</div>
								</div>
							</div>
                        <div className="row form_footer account_form_footer">
                            <div className="col-sm-12">
                                <div className="signup_btn">
                                    <button type="button" className="btn btn-primary" data-toggle="modal" data-target="#summary_modal">
                                        Continue to Payment
                                        <span className="btn_arrow"><i className="fas fa-arrow-circle-right"></i></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

	
        ); 

    }
}
export default BuyPaymentBox;