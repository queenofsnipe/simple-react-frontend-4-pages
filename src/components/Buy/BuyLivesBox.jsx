import React, { Component } from "react";
export class BuyLivesBox extends Component{
    constructor(props) {
        super(props);
        this.state = {
            currency : this.props.currency,
            items:this.props.items,
            price:this.props.price
        };
    }
    render(){
        return (
            <div className="buy_lives_box">
                <div className="buy_lives_top">
                    <h4><i className="fas fa-heart"></i></h4>
                    <h3>{ this.state.items } Lives</h3>
                </div>
                <div className="buy_lives_bottom">
                    <h3>{this.state.currency + " " + this.state.items}</h3>
                    <p>{"Purchase "+this.state.items+" Lives"}</p>
                </div>
            </div>
	
        ); 

    }
}
export default BuyLivesBox;