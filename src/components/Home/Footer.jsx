import React, { Component } from "react";
import logo from "assets/images/logo.png";


export class Footer extends Component{
    constructor(props){
        super(props);
        this.state={

        }
    }

    render(){
        return(
            <footer className="site_footer">
                <div className="container">
                    <div className="row">
                        <div className="col-sm-12">
                            <nav className="navbar navbar-expand-lg">
                                <a className="navbar-brand" href="#"><img src={logo} alt="img"/></a>
        
                                <div className="navbar-collapse">
                                    <ul className="navbar-nav m-auto">
                                        <li className="nav-item">
                                            <a className="nav-link" href="#">Terms of Play</a>
                                        </li>
                                        <li className="nav-item">
                                            <a className="nav-link" href="#">Privacy Policy</a>
                                        </li>
                                        <li className="nav-item">
                                            <a className="nav-link" href="#">Contact</a>
                                        </li>
                                    </ul>
                                </div>
        
                                <div className="socialMediaIconsFooter">
                                    <ul style={{display: 'inline-block'}}>
                                        <li className="socialMediaIconFooter socialMediaIconFooterNone">
                                            <a href="#">
                                                <i className="fab fa-facebook-square"></i>
                                            </a>
        
                                        </li>
                                        <li className="socialMediaIconFooter">
                                            <a href="#">
                                                <i className="fab fa-instagram"></i>
                                            </a>
                                        </li>
                                        <li className="socialMediaIconFooter">
                                            <a href="#">
                                                <i className="fab fa-youtube-square"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
        
                            </nav>
                        </div>
                    </div>
                </div>
            </footer>
        );
    }
}

export default Footer;