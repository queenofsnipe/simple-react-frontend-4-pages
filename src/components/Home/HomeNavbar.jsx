import React, { Component } from "react";
import logo from "assets/images/logo.png"

export class HomeNavbar extends Component{

    render(){
        return(
            <nav className="navbar navbar-expand-lg">
                <a className="navbar-brand" href="/admin/index"><img src={ logo } alt="logo"/></a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
                </button>

                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item">
                            <a className="nav-link" href="/admin/buy">Buy Lives</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#play_area">How to Play</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#aboutus_area">About</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#">Contact</a>
                        </li>
                    </ul>
                    <ul className="navbar-nav ml-auto">
                        <li className="nav-item">
                            <a className="nav-link" href="#">Invite a friend <i className="fas fa-user-plus"></i></a>
                        </li>
                        <li className="nav-item nav_btn">
                            <button type="button" className="btn" data-toggle="modal" data-target="#creat_account">My account</button>
                        </li>
                        <li className="nav-item nav_like">
                            <button type="button" className="btn">3 <i className="fas fa-heart"></i></button>
                        </li>
                    </ul>
                </div>
            </nav>
        )
    }
}
export default HomeNavbar;