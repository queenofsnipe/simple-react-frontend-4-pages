import React, { Component } from "react";
import appstore from "assets/images/app-store.png";
import googleplay from "assets/images/google-play.png";


export class AppArea extends Component{
    constructor(props){
        super(props);
        this.state={

        }
    }

    render(){
        return(
            <section className="apps_area">
                <div className="container">
                    <div className="row">
                        <div className="col-sm-12">
                            <h2>Your fate is in your hands…</h2>
                            <ul>
                                <li>
                                    <a href="#"><img src={ appstore } alt="img"/></a>
                                </li>
                                <li>
                                    <a href="#"><img src={ googleplay } alt="img"/></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default AppArea;