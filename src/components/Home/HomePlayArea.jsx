import React, { Component } from "react";
import download from "assets/images/download.png";
import howtoplay from "assets/images/howtoplay.png";
import money from "assets/images/money.png";
import hearts from "assets/images/hearts.png";



export class HomePlayArea extends Component{
    constructor(props){
        super(props);
        this.state={

        }
    }

    render(){
        return(
            <section className="play_area" id="play_area">
                <div className="container">
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="play_content">
                                <h3>How to play</h3>
                                <p>
                                    Become the Last Won Standing and win weekly cash prizes by training and competing in a series of skills based games.
                                </p>
                                <h5>Live games are held every Wednesday and Saturday at 8pm.</h5>
                            </div>
                            <div className="play_list">
                                <ul>
                                    <li>
                                        <div className="media">
                                            <img className="d-flex align-self-center play_icon" src={download} alt="icon"/>
                                            <div className="media-body align-self-center">
                                                <p>
                                                    Begin by downloading the APP <br/> and creating an account.
                                                </p>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="media">
                                            <img className="d-flex align-self-center play_icon" src={howtoplay} alt="icon"/>
                                            <div className="media-body align-self-center">
                                                <p>
                                                    Then use the APP to train whenever you like in a series of games and master your skills.
                                                </p>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="media">
                                            <img className="d-flex align-self-center play_icon" src={money} alt="icon"/>
                                            <div className="media-body align-self-center">
                                                <p>
                                                    You need to purchase lives to compete for the cash prizes in the live games.
                                                </p>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="media">
                                            <img className="d-flex align-self-center play_icon" src={hearts} alt="icon"/>
                                            <div className="media-body align-self-center">
                                                <p>
                                                    Lives are priced at £1 per life. You can purchase lives through our website and then use those lives to compete in the weekly live games!
                                                </p>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default HomePlayArea;