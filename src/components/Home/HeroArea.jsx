import React, { Component } from "react";
import googleplay from "assets/images/google-play.png"
import appstore from "assets/images/app-store.png"
import hero_img from "assets/images/img-1.png"

export class HeroArea extends Component{
    constructor(props){
        super(props);
        this.state={

        }
    }

    render(){
        return(
            <section className="hero_area">
                <div className="container">
                    <div className="row">
                        <div className="col-md-6">
                            <div className="hero_content">
                                <h4>
                                    The Skills Based App That Puts Your Fate In Your Hands…
                                </h4>
                                <ul>
                                    <li>
                                        <a href="#"><img src={ appstore } alt="img"/></a>
                                    </li>
                                    <li>
                                        <a href="#"><img src={googleplay} alt="img"/></a>
                                    </li>
                                </ul>
                                
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="hero_img">
                                <img src={hero_img} alt="img"/>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default HeroArea;