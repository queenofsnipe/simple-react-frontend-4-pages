
import React, { Component } from "react";
import game1 from "assets/images/game1.png";
import game2 from "assets/images/game2.png";


export class HomeAboutUsArea extends Component{
    constructor(props){
        super(props);
        this.state={

        }
    }

    render(){
        return(
            <section className="aboutUs_area" id="aboutus_area">
                <div className="container">
                    <div className="row">
                        <div className="col-md-4">
                            <div className="about_titles">
                                <h4>About us</h4>
                                <p>
                                    Our story, and why we do what we do.
                                </p>
                            </div>
                        </div>
                        <div className="col-md-8">
                            <div className="about_content">
                                <p>
                                    Last Won Standing is the skills based APP game where you can play weekly skills APP based competitions to win cash prizes. 
                                </p>
                                <p>
                                    We love handing out prizes to our winners, helping them to purchase the things they want in life. 
                                </p>
                                <p>
                                    Prizes are won by coming in the top three during the live games.
                                </p>
                                <p>
                                    Just imagine… An app that lets you train to master your skills every day and whenever you are ready you can compete in the live games.
                                </p>
                                <p>
                                    The best part for us is ringing our customers after the live game has finished and telling them they were the Last Won Standing and coming to their home to present them with a cheque or a deposit into their bank account.
                                </p>
                                <p>
                                    We also support a number of UK registered charities as we know giving in life is the best part of life.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="game_bg_1">
                    <img src={game1} alt="img"/>
                </div>
                <div className="game_bg_2">
                    <img src={game2} alt="img"/>
                </div>
            </section>
        );
    }
}

export default HomeAboutUsArea;