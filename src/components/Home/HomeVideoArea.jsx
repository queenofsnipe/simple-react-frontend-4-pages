import React, { Component } from "react";
import videoplay from "assets/images/video-play.png";
import videopic from "assets/images/img-2.png";


export class HomeVideoArea extends Component{
    constructor(props){
        super(props);
        this.state={

        }
    }

    render(){
        return(
            <section className="video_area">
                <div className="container">
                    <div className="row">
                        <div className="col-md-4">
                            <div className="video_content">
                                <h4>
                                    Watch this Video
                                </h4>
                                <p>
                                    See how simple it is to get started and start winning.
                                </p>
                                <a href="#">
                                    <img src={ videoplay } alt="img"/>
                                </a>
                            </div>
                        </div>
                        <div className="col-md-8">
                            <div className="video_pic">
                                <img src={videopic} alt="img"/>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default HomeVideoArea;