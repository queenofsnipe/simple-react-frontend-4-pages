import React, { Component } from "react";


export class HomeCreateAccountModal extends Component{
    constructor(props){
        super(props);
        this.state={
            
        }
    }

    render(){
        return(
            <div className="modal fade creat_account_modal" id="creat_account" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                    <h5 className="modal-title" id="exampleModalLabel">Create an account</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div className="modal-body">
                    <div className="account_form">
                        <form>
                            <div className="row">
                                <div className="col-sm-6">
                                    <div className="form-group">
                                        <label htmlFor="first-name">First name</label>
                                        <input type="text" className="form-control" id="first-name" placeholder="First name" />
                                    </div>
                                </div>
                                <div className="col-sm-6">
                                    <div className="form-group">
                                        <label htmlFor="last-name">Last name</label>
                                        <input type="text" className="form-control" id="last-name" placeholder="Last name"/>
                                    </div>
                                </div>
                                <div className="col-sm-6">
                                    <div className="form-group">
                                        <label htmlFor="Email1">Email Address</label>
                                        <input type="email" className="form-control" id="Email1" placeholder="Enter email"/>
                                    </div>
                                </div>
                                <div className="col-sm-6">
                                    <div className="form-group">
                                        <label htmlFor="Date-of-Birth">Date of Birth</label>
                                        <input type="date" className="form-control" id="Date-of-Birth" placeholder="Date of Birth"/>
                                    </div>
                                </div>
                                <div className="col-sm-6">
                                    <div className="form-group">
                                        <label htmlFor="Mobile-Number">Mobile Number</label>
                                        <input type="text" className="form-control" id="Mobile-Number" placeholder="Mobile Number"/>
                                    </div>
                                </div>
                                <div className="col-sm-6">
                                    <div className="form-group">
                                        <label htmlFor="Username">Username</label>
                                        <input type="text" className="form-control" id="Username" placeholder="Username" />
                                    </div>
                                </div>
                                <div className="col-sm-6">
                                    <div className="form-group">
                                        <label htmlFor="Password">Password</label>
                                        <input type="password" className="form-control" id="Password" placeholder="Password" />
                                    </div>
                                </div>
                                <div className="col-sm-6">
                                    <div className="form-group">
                                        <label htmlFor="Confirm-Password">Confirm Password</label>
                                        <input type="password" className="form-control" id="Confirm-Password" placeholder="Confirm Password" />
                                    </div>
                                </div>
                            </div>
                            <div className="row form_footer">
                                <div className="col-sm-6">
                                    <div className="form-group form-check">
                                        <input type="checkbox" className="form-check-input" id="exampleCheck1"/>
                                        <label className="form-check-label" htmlFor="exampleCheck1">
                                        I accept the <a href="#">privacy policy</a>
                                        <br/>
                                        and <a href="#">terms of play</a>
                                        </label>
                                    </div>
                                </div>
                                <div className="col-sm-6">
                                    <div className="signup_btn">
                                        <button type="submit" className="btn btn-primary">Sign up <i className="fas fa-arrow-circle-right"></i></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    </div>
                </div>
                </div>
            </div>
        );
    }
}

export default HomeCreateAccountModal;