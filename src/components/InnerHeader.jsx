import React, { Component } from "react";
import logo_black from "assets/images/logo-black.png";
import nav_toggle from "assets/images/nav_toggle.svg";
import user from "assets/images/user.png";

export class InnerHeader extends Component {
  render() {
    return (
      <section className="inner_header">
        <div className="inner_nav_brand">
          <a href="#">
            <img src={ logo_black} alt="img"/>
          </a>
        </div>
        <div className="inner_toggle">
          <img src={nav_toggle} alt="img"/>
        </div>
        <div className="inner_header_nav">
        <ul>
          <li>
            <a href="#"><b>3</b> <i className="fas fa-heart"></i></a>
          </li>
          <li>
            <a href="#">Welcome Daniel <img className="nav_user" src={ user } alt="img"/></a>
          </li>
        </ul>
      </div>
      </section>
    );
  }
}


export default InnerHeader;
