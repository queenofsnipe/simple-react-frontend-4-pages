import React, { Component } from "react";

export class AccountMainArea extends Component{
    constructor(props){
        super(props);
        this.state={

        }
    }

    render(){
        return(
            <div className="inner_main_area">
                <div className="account_box">
                    <div className="account_title">
                        <h4>Account Details</h4>
                    </div>
                    <div className="account_form">
                        <form>
                            <div className="row">
                                <div className="col-sm-6">
                                    <div className="form-group">
                                        <label htmlFor="first-name">First name</label>
                                        <input type="text" className="form-control" id="first-name" placeholder="First name" />
                                    </div>
                                </div>
                                <div className="col-sm-6">
                                    <div className="form-group">
                                        <label htmlFor="last-name">Last name</label>
                                        <input type="text" className="form-control" id="last-name" placeholder="Last name" />
                                    </div>
                                </div>
                                <div className="col-sm-6">
                                    <div className="form-group">
                                        <label htmlFor="Email1">Email Address</label>
                                        <input type="email" className="form-control" id="Email1" placeholder="Enter email" />
                                    </div>
                                </div>
                                <div className="col-sm-6">
                                    <div className="form-group">
                                        <label htmlFor="Date-of-Birth">Date of Birth</label>
                                        <input type="date" className="form-control" id="Date-of-Birth" placeholder="Date of Birth"/>
                                    </div>
                                </div>
                                <div className="col-sm-6">
                                    <div className="form-group">
                                        <label htmlFor="Mobile-Number">Mobile Number</label>
                                        <input type="text" className="form-control" id="Mobile-Number" placeholder="Mobile Number" />
                                    </div>
                                </div>
                                <div className="col-sm-6">
                                    <div className="form-group">
                                        <label htmlFor="Username">Username</label>
                                        <input type="text" className="form-control" id="Username" placeholder="Username"/>
                                    </div>
                                </div>
                                <div className="col-sm-6">
                                    <div className="form-group">
                                        <label htmlFor="Password">Password</label>
                                        <input type="password" className="form-control" id="Password" placeholder="Password"/>
                                    </div>
                                </div>
                                <div className="col-sm-6">
                                    <div className="form-group">
                                        <label htmlFor="Confirm-Password">Confirm Password</label>
                                        <input type="password" className="form-control" id="Confirm-Password" placeholder="Confirm Password"/>
                                    </div>
                                </div>
                            </div>
                            <div className="row form_footer account_form_footer">
                                <div className="col-sm-12">
                                    <div className="signup_btn">
                                        <button type="submit" className="btn btn-primary">
                                            Save
                                            <span className="btn_arrow"><i className="fas fa-arrow-circle-right"></i></span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default AccountMainArea;