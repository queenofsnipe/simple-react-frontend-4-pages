import React, { Component } from "react";

export class HistoryMainArea extends Component{
    constructor(props){
        super(props);
        this.state={

        }
    }

    render(){
        return(
            <div className="inner_main_area">
            <div className="inner_area_box">
                <div className="history_table">
                    <table border="0" cellspacing="0" cellpadding="0" align="center">
                        <thead>
                            <tr>
                                <td >
                                    Date
                                </td>
                                <td >
                                    Title of Order
                                </td>
                                <td >
                                    Lifes
                                </td>
                                <td >
                                    Price
                                </td>
                            </tr>
                            
                        </thead>
                        
                        <tbody>
                            <tr>
                                <td >
                                    12/05/2019
                                </td>
                                <td >
                                    Purchase 4 Lifes
                                </td>
                                <td >
                                    4
                                </td>
                                <td >
                                    £4
                                </td>
                            </tr>
                            <tr>
                                <td >
                                    12/05/2019
                                </td>
                                <td >
                                    Purchase 4 Lifes
                                </td>
                                <td >
                                    4
                                </td>
                                <td >
                                    -
                                </td>
                            </tr>
                            <tr>
                                <td >
                                    12/05/2019
                                </td>
                                <td >
                                    Purchase 4 Lifes
                                </td>
                                <td >
                                    4
                                </td>
                                <td >
                                    £4
                                </td>
                            </tr>
                            <tr>
                                <td >
                                    12/05/2019
                                </td>
                                <td >
                                    Purchase 4 Lifes
                                </td>
                                <td >
                                    4
                                </td>
                                <td >
                                    £4
                                </td>
                            </tr>
                            <tr>
                                <td >
                                    12/05/2019
                                </td>
                                <td >
                                    Purchase 4 Lifes
                                </td>
                                <td >
                                    4
                                </td>
                                <td >
                                    £4
                                </td>
                            </tr>
                            <tr>
                                <td >
                                    12/05/2019
                                </td>
                                <td >
                                    Purchase 4 Lifes
                                </td>
                                <td >
                                    4
                                </td>
                                <td >
                                    £4
                                </td>
                            </tr>
                            <tr>
                                <td >
                                    12/05/2019
                                </td>
                                <td >
                                    Purchase 4 Lifes
                                </td>
                                <td >
                                    4
                                </td>
                                <td >
                                    £4
                                </td>
                            </tr>
                            <tr>
                                <td >
                                    12/05/2019
                                </td>
                                <td >
                                    Purchase 4 Lifes
                                </td>
                                <td >
                                    4
                                </td>
                                <td >
                                    £4
                                </td>
                            </tr>
                            <tr>
                                <td >
                                    12/05/2019
                                </td>
                                <td >
                                    Purchase 4 Lifes
                                </td>
                                <td >
                                    4
                                </td>
                                <td >
                                    £4
                                </td>
                            </tr>
                            <tr>
                                <td >
                                    12/05/2019
                                </td>
                                <td >
                                    Purchase 4 Lifes
                                </td>
                                <td >
                                    4
                                </td>
                                <td >
                                    £4
                                </td>
                            </tr>
                            <tr>
                                <td >
                                    12/05/2019
                                </td>
                                <td >
                                    Purchase 4 Lifes
                                </td>
                                <td >
                                    4
                                </td>
                                <td >
                                    £4
                                </td>
                            </tr>
                            <tr>
                                <td >
                                    12/05/2019
                                </td>
                                <td >
                                    Purchase 4 Lifes
                                </td>
                                <td >
                                    4
                                </td>
                                <td >
                                    £4
                                </td>
                            </tr>
                        </tbody>
    
                    </table>
                </div>
            </div>
        </div>
        );
    }
}

export default HistoryMainArea;