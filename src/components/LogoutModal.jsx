import React, { Component } from "react";
export class LogoutModal extends Component{
    constructor(props) {
        super(props);
        this.state = {
        };
    }
    render(){
        return (
            <div className="modal fade logut_modal" id="logut_modal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-body">
                            <div className="logout_title">
                                <h5>Logout</h5>
                                <p>Are you sure you want to logout?</p>
                                <div className="logout_btns">
                                    <button type="button" className="btn logout_cancle">Cancel</button>
                                    <button type="button" className="btn logout_log">Logout</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
	
        ); 

    }
}
export default LogoutModal;